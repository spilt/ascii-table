PREFIX=
CC=cc
CFLAGS=-O3 -std=gnu99
LIBS=-lncurses -lm

all: ascii

clean:
	rm ascii

ascii: ascii.c
	$(CC) ascii.c $(LIBS) $(CFLAGS) -o ascii
	
install: ascii
	@prefix="$(PREFIX)"; \
	if [[ ! $$prefix ]]; then \
		read -p $$'\033[1mWhere do you want to install? (default: /usr/local) \033[0m' prefix; \
	fi; \
	if [[ ! $$prefix ]]; then \
		prefix="/usr/local"; \
	fi; \
	mkdir -pv $$prefix/bin $$prefix/share/man/man1 \
	&& cp -v ascii $$prefix/bin/ \
	&& cp -v doc/ascii.1 $$prefix/share/man/man1/

uninstall:
	@prefix="$(PREFIX)"; \
	if [[ ! $$prefix ]]; then \
		read -p $$'\033[1mWhere do you want to uninstall from? (default: /usr/local) \033[0m' prefix; \
	fi; \
	if [[ ! $$prefix ]]; then \
		prefix="/usr/local"; \
	fi; \
	echo "Deleting..."; \
	rm -rvf $$prefix/bin/ascii $$prefix/share/man/man1/ascii.1

