# ascii

A simple curses console application that shows an ASCII table and lets you
manually output ASCII sequences.

![Screenshot](https://bitbucket.org/spilt/ascii-table/downloads/ascii_screenshot.gif)

## Usage

Just run `ascii` to launch the ascii table viewier. Any text piped in will be
appended to an output buffer, followed by the ASCII values of any numbers
passed as arguments.

## Controls
In the ASCII table viewer, you can navigate with arrow keys or h/j/k/l.
Pressing `enter` will append the selected character to the output buffer and
`backspace` will delete from the output buffer.

Pressing `/` will open a search. You can begin typing to search by character or
by name, and hit `enter` to select the result or `escape` to cancel. After
selecting a result, you can hit `n` to jump to the next match or `N` to jump to
the previous match.

Pressing `i` will enter insert mode, where you can type freely into the output
buffer, pressing `escape` will leave insert mode. Pressing `q` or `escape` will
exit and print the output buffer to stdout. Hitting control-c will quit without
printing the output buffer.
